from setuptools import setup

version = '0.3'

install_requires = [
    'pyramid',
    'tweepy',
    'fbconsole',
    'google-api-python-client'
    ]

setup(name="pyramid-social-feed",
      version=version,
      description="Test feed project based on Pyramid Framework",
      author="Max Power",
      author_email="yakudzam@gmail.com",
      install_requires=install_requires,
      package_data={
        },
      license="Apache 2.0",
      keywords="pyramid social feed",
      classifiers=["Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Framework :: Pyramid",
        "Environment :: Web Environment",])