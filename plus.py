# -*- coding: utf-8 -*-
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.tools import run
from oauth2client.file import Storage
from datetime import datetime
import apiclient.discovery
import httplib2
import os.path
import settings


def authorize_self(client_id=None, client_secret=None):
    if client_id is None or client_secret is None:
        raise Exception('Please register at the API Console at: https://code.google.com/apis/console. \
         See README.txt for details!')

    flow = OAuth2WebServerFlow(
        client_id=client_id,
        client_secret=client_secret,
        scope='https://www.googleapis.com/auth/plus.me',
        user_agent='google-api-client-python-plus-cmdline/1.0',
        xoauth_displayname='Google Plus Client Example App'
    )
    credentials_file = 'plus_auth.dat'

    storage = Storage(credentials_file)
    if os.path.exists(credentials_file):
        credentials = storage.get()
    else:
        credentials = run(flow, storage)
    return credentials


def build_service(credentials, http, api_key=None):
    if (credentials != None):
        http = credentials.authorize(http)
    service = apiclient.discovery.build('plus', 'v1', http=http, developerKey=api_key)
    return service


def plus():
    http = httplib2.Http()
    credentials = authorize_self(settings.CLIENT_ID, settings.CLIENT_SECRET)
    service = build_service(credentials, http)
    person = service.people().get(userId='me').execute(http)
    httpUnauth = httplib2.Http()
    serviceUnauth = build_service(None, httpUnauth, settings.API_KEY)
    request = serviceUnauth.activities().list(userId=person['id'], collection='public')
    activities = []

    while ( request != None ):
        activities_doc = request.execute(httpUnauth)
        if 'items' in activities_doc:
            activities += activities_doc['items']
        # We're using Python's built-in pagination support
        request = serviceUnauth.activities().list_next(request, activities_doc)

    if len(activities) > 0:
        for item in activities:
            data = [str(datetime.now())] + ['activity'] + [item['object']['content'][:200]] + [item['id']]
            yield ' '.join(data)