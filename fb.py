# -*- coding: utf-8 -*-
import os
import fbconsole
from datetime import datetime
here = os.path.dirname(os.path.abspath(__file__))


def fbfeed():
    fbconsole.APP_ID = '588914247790498'
    fbconsole.AUTH_SCOPE = ['publish_stream', 'publish_checkins', 'read_stream', 'offline_access']
    fbconsole.authenticate()
    newsfeed = fbconsole.get('/me/home')
    newsfeedData = newsfeed["data"]
    for status in newsfeedData:
        time = str(datetime.now())
        fromn = [status['from']['name']]
        name = status.get('name', None)
        description = status.get('description', None)
        if name:
            fromn.append(name)
        if description:
            fromn.append(description)
        if time:
            fromn.insert(0, time)
        yield ' '.join(fromn)


