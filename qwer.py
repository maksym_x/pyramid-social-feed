# -*- coding: utf-8 -*-
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from fb import fbfeed
from plus import plus
from datetime import datetime
import tweepy
import os

consumer_key = ""
consumer_secret = ""
access_key = ""
access_secret = ""
here = os.path.dirname(os.path.abspath(__file__))


def twitterfeed():
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)
    statuses = tweepy.Cursor(api.home_timeline).items(20)
    data = ["%s %s" % (datetime.now(), s.text.encode('utf8')) for s in statuses]
    return data


def main(request):
    twitter = twitterfeed()
    facebook = [s.encode('utf8') for s in fbfeed()]
    googleplus = [s.encode('utf8') for s in plus()]
    feed = twitter + facebook + googleplus
    data = sorted(feed)
    return {"data": data}


if __name__ == '__main__':
    settings = {}
    settings['reload_all'] = True
    settings['debug_all'] = True
    settings['mako.directories'] = os.path.join(here, 'templates')
    config = Configurator(settings=settings)
    config.add_view(main, route_name='list', renderer='list.mako')
    config.add_route('list', '/')
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8000, app)
    server.serve_forever()
