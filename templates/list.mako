# -*- coding: utf-8 -*- 
<%inherit file="layout.mako"/>

<h1>Feed List</h1>

<ul id="data">
% if data:
  % for status in data:
  <li>
       <div class="section">
          <div class="article-brief">
             <div class="post">
                <div class="post-time"> </div>
                   <p>${status.decode("utf-8")}</p>
             </div>
          </div>
       </div>
  </li>
  % endfor
% endif
  
</ul>